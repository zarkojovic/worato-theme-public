(function($) {
    //Jumbotron fullscreen size
    $(document).ready(function() {
        resizeDiv();
    });
    window.onresize = function(event) {
        resizeDiv();
    };

     $(window).scroll(function() {
        home_navbar_toggle();
    });


    function resizeDiv() {
            home_factor = 0;
            if ( $(window).width() > 768 ) { home_factor = 0 } else { home_factor = 50 };
            vph = $(window).height() + 1 - home_factor; //-50 for the navbar +1 just in case
            $('.homepage-jumbotron').css({
                'min-height': vph + 'px'
            });
        }
        //Jumbotron scroll button click (there is also hide function bellow to hide it after scroll)
    var height_jumbotron_scroll = $(window).height() - 50; //50 for the navbar height
    $('a.see-more-button').click(function(event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: height_jumbotron_scroll
        }, 400);
    });

    //Navigation scroll effect
   function home_navbar_toggle() {
        if ($(window).scrollTop() > 100) {
            $('nav.navbar-home').addClass('nav-scrolled');
        }
        else {
            $('nav.navbar-home').removeClass('nav-scrolled');
        }
   };

})(jQuery);
