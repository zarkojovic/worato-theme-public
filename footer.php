<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Worato
 */

?>

	</div><!-- #content -->

	<footer id="main-footer" class="site-footer" role="contentinfo">
		<div class="container">
			<div class="row footer-columns-row">
				<div class="col-xs-6 col-sm-4 col-md-2 footer-column footer-column-1">
					<?php dynamic_sidebar( 'footer-column-1' );?>
				</div>
				<div class="col-xs-6 col-sm-4 col-md-2 footer-column footer-column-2">
					<?php dynamic_sidebar( 'footer-column-2' );?>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-2 footer-column footer-column-3">
					<?php dynamic_sidebar( 'footer-column-3' );?>
				</div>
				<div class="col-xs-12 col-md-2 footer-column footer-column-4">
					<?php dynamic_sidebar( 'footer-column-4' );?>
				</div>
				<div class="col-xs-12 col-md-4 footer-column footer-column-5">
					<p>newsletter form</p>
					<?php dynamic_sidebar( 'footer-column-5' );?>
				</div>
			</div>
		</div>
	</footer><!-- #main-footer -->
	
	<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<p>© 2014 - <?php echo date("Y");?> – WORATO</p>
	</div>
	<!--a class="back-to-top" href="#page" title="Back to to top"><img src="<?php echo get_template_directory_uri() . '/assets/icons/icons-themeservedshop-22.svg' ?>"></a -->
</footer><!-- #colophon -->
	
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
