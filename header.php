<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Worato
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<?php if ( is_front_page() ) { $navbarclass='navbar-home'; } else { $navbarclass=''; } ?>
	<nav class="navbar navbar-default navbar-fixed-top <?php echo $navbarclass; ?>">
		<div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?php bloginfo( 'url' ); ?>"><img src="<?php echo get_bloginfo('template_directory');?>/assets/img/worato-logo2.svg"></a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <?php
			wp_nav_menu( array(
				'menu'				=> 'Primary',
				'theme_location'	=> 'primary',
				'depth'				=> 2,
				'container'			=> 'div',
				'container_class'	=> 'collapse navbar-collapse',
				'container_id'		=> 'primary-menu',
				'menu_class'		=> 'nav navbar-nav navbar-right',
				'fallback_cb'		=> 'wp_bootstrap_navwalker::fallback',
				'walker'			=> new wp_bootstrap_navwalker())
			);

			wp_nav_menu( array(
				'menu'				=> 'Mobile',
				'theme_location'	=> 'mobile',
				'depth'				=> 2,
				'container'			=> 'div',
				'container_class'	=> 'collapse navbar-collapse ',
				'container_id'		=> 'mobile-menu',
				'menu_class'		=> 'nav navbar-nav navbar-right',
				'fallback_cb'		=> 'wp_bootstrap_navwalker::fallback',
				'walker'			=> new wp_bootstrap_navwalker())
			);
        ?>
	  </div><!-- /.container -->
	</nav>

	<div id="content" class="site-content">
