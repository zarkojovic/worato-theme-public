<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Worato
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="homepage-jumbotron"
			<?php
				if (has_post_thumbnail( $post->ID ) ) :
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );?>
					style="background-image: url('<?php echo $image[0]; ?>')";
			<?php endif; ?>
		>
			<div class="jumbotron-content-wrapper">
				<div class="jumbotron-imagery container">
					<img src="<?php echo get_template_directory_uri() . '/assets/img/worato-home-bckg.svg'; ?>" alt="">
				</div>
				<h1>QR & barcode revolution</h1>
				<p class="big">We are revolutionizing tech world by building small and simple<br class="big hidden-xs hidden-sm" /> communities around QR and barcodes.</p>
				<a class="see-more-button" href=""><img src="<?php echo get_template_directory_uri() . '/assets/img/scroll-down.svg'; ?>" alt=""></a>
			</div>
		</div>

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'worato' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'worato' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->
