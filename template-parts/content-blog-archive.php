<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Worato
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;

			if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php worato_posted_on(); ?> <?php worato_entry_footer(); ?>
			</div><!-- .entry-meta -->
			<?php
			endif; ?>
			<div class="blog-archive-featured">
					<?php if ( has_post_thumbnail() ) : ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								<?php the_post_thumbnail(); ?>
						</a>
				<?php endif; ?>
			</div>
		</header><!-- .entry-header -->

		<div class="entry-content">
				<?php
					the_excerpt();
				?>
		</div><!-- .entry-content -->

		<footer class="entry-footer">
			<a href="<?php the_permalink(); ?>" class="blog-read-more"><button>Read More</button></a>
		</footer><!-- .entry-footer -->
</article><!-- #post-## -->
